#! /usr/bin/env python

import os
import sys

def build():

    cmd = "mvn package -DskipTests"
    print(cmd)
    os.system(cmd)

def install():
    cmd = "cp -f target/*.jar bin/ebeim-server.jar"
    print(cmd)
    os.system(cmd)

def setup():
    build()
    install()

if __name__ == '__main__':
    setup()
