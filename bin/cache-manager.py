#! /usr/bin/python
import os

CACHE_DIR = "/data/ebeim/working"

def clean_cache():
    cmd = "find " + CACHE_DIR + " -atime +0 -size -5368709120c -type f -exec rm -f {} \;"
    os.system(cmd)

if __name__ == '__main__':
    print('clean_cache:' + CACHE_DIR)
    clean_cache()
