package cn.nawang.ebeim.server;

import cn.nawang.ebeim.server.constants.Config;
import cn.nawang.ebeim.server.service.ZookeeperService;
import cn.nawang.ebeim.server.thread.FolderScanner;
import cn.nawang.ebeim.server.transfer.TransferServer;
import cn.nawang.ebeim.server.util.ThreadPoolUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppMain {

    private static final Logger LOG = LoggerFactory.getLogger(AppMain.class);

    public static void main(String[] args) {
        try {
            Config.init();
            LOG.info("begin start main function.");
            // 初始化接收客户端上传请求
            ThreadPoolUtil.submitTask(new TransferServer());
            // 扫描待上传文件线程
            ThreadPoolUtil.submitTask(new FolderScanner());
            // 处理上传文件线程
            ThreadPoolUtil.startUploadFileThread();
            // 注册Zookeeper服务
            ZookeeperService.instance().register();
            LOG.info("main function start success.");
        } catch (Exception ex) {
            LOG.error("init server failed!" ,ex);
            System.exit(0);
        }
    }

}
