package cn.nawang.ebeim.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by GanJc on 2015-11-10 17:34
 */
public class LocalStorageService implements StorageService {

    private static final Logger LOG = LoggerFactory.getLogger(LocalStorageService.class);

    public void uploadFile(String dataSourceId, String signature,String uploadPath) {
        //TODO 将文件从临时文件夹下 移动动到本地存储路径下
    }

    public void downloadFile(String dataSourceId, String signature,String downloadPath) {
        //TODO 将文件从本地存储路径下 移动到下载路径下
    }

}
