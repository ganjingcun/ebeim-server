package cn.nawang.ebeim.server.service;

import cn.nawang.ebeim.server.constants.Config;

/**
 * Created by GanJc on 2015-11-10 17:34
 */
public class StorageServiceFactory {

    private static StorageService storageService ;

    public static StorageService getStorageService() {
        if (storageService == null) {
            synchronized (StorageServiceFactory.class) {
                if (Config.STORAGE_UTIL_TYPE.equals("swift")) {
                    storageService = new SwiftStorageService();
                } else if (Config.STORAGE_UTIL_TYPE.equals("local")) {
                    storageService = new LocalStorageService();
                }
            }
        }
        return storageService;
    }

}
