package cn.nawang.ebeim.server.service;

/**
 * Created by GanJc on 2015-11-10 17:34
 */
public interface StorageService {

    /**
     * 本地上传到服务器（File）
     *
     * @param dataSourceId
     * @param signature
     */
    void uploadFile(String dataSourceId, String signature, String uploadPath);

    /**
     * 从服务器上下载到本地（File）
     *
     * @param dataSourceId
     * @param signature
     */
    void downloadFile(String dataSourceId, String signature, String downloadPath);

}
