package cn.nawang.ebeim.server.service;

import cn.nawang.ebeim.server.constants.Config;
import cn.nawang.ebeim.server.constants.Constant;
import org.apache.zookeeper.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;

/**
 * 封装zookeeper客户端
 * 提供zookeeper注册，节点查询操作
 * (主要是为了解决之前存在的递归创建连接的弊端)
 * Created by GanJc on 2015-11-20 08:56
 */
public class ZookeeperService implements Watcher{

    private static final Logger LOG = LoggerFactory.getLogger(ZookeeperService.class);

    private static ZookeeperService zookeeperService = new ZookeeperService();

    private ZooKeeper zooKeeper;

    private CountDownLatch countDownLatch ;

    private ZookeeperService() {
        init();
    }

    public static ZookeeperService instance() {
        return zookeeperService;
    }

    /**
     *  每次调用初始化init(),则释放旧连接内存资源，
     *  创建新的zookeeper连接
     */
    private void init() {
        LOG.info("zookeeper init!");
        try {
            if (zooKeeper != null) {
                zooKeeper.close();
                zooKeeper = null ;
            }
            countDownLatch = new CountDownLatch(1);
            String zkHosts = ApiService.getInstance().getZookeeperHosts();
            int sessionTime = Config.SESSION_TIME;
            zooKeeper = new ZooKeeper(zkHosts, sessionTime, this);
            countDownLatch.await();
        } catch (Exception e) {
            LOG.error("zookeeper服务器不可用：5秒后重试!", e);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e1) {
                LOG.error("InterruptedException:",e1);
            }
            init();
        }finally {
            countDownLatch = null ;
        }
    }

    public void process(WatchedEvent event) {
        switch (event.getState()) {
            case SyncConnected:
                if (countDownLatch != null) {
                    countDownLatch.countDown();
                }
                break;
            case Expired:
                LOG.info("zookeeper session expire,reconnect!" );
                init();
                register();
                break;
        }
    }

    public void register() {
        try {
            if (!exists(Constant.ZK_GROUP_ROOT)) {
                create(Constant.ZK_GROUP_ROOT);
            }
            String nodePath = Constant.ZK_GROUP_ROOT + "/" + Config.HOST_NAME;
            createNodeWithContent(nodePath);
            LOG.info("register zookeeper success - {}", nodePath);
        } catch (KeeperException e) {
            LOG.error("register zookeeper failed, retry 5s later..", e);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            init();
            register();
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    private void create(String path, byte[] data) throws KeeperException, InterruptedException {
        zooKeeper.create(path, data, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);
    }

    private void create(String path) throws KeeperException, InterruptedException {
        zooKeeper.create(path, "".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
    }

    private boolean exists(String nodePath) throws KeeperException, InterruptedException {
        if (zooKeeper.exists(nodePath, true) != null) {
            return true;
        }
        return false;
    }

    private void createNodeWithContent(String path) throws KeeperException, InterruptedException {
        //获取节点数据 改为从配置文件里取 ，在那台服务器启动server 就在哪台服务器的配置文件 配置该server的 外网地址
        String nodeContent = Config.UPLOAD_HOST + ":" + Config.UPLOAD_PORT;
        //创建节点和节点数据
        create(path, nodeContent.getBytes());
        //监听此节点
        exists(path);
    }

}
