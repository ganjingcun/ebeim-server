package cn.nawang.ebeim.server.task;

/**
 * Created by GanJc on 2015-11-11 15:16
 */
public interface Task {
    void execute();
}
