package cn.nawang.ebeim.server.entity;

public class StorageServer {

    private String id;

    private String host;

    private int port;

    private String storageCloudId;

    private String internalHost;

    private int internalPort;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getStorageCloudId() {
        return storageCloudId;
    }

    public void setStorageCloudId(String storageCloudId) {
        this.storageCloudId = storageCloudId;
    }

    public String getInternalHost() {
        return internalHost;
    }

    public void setInternalHost(String internalHost) {
        this.internalHost = internalHost;
    }

    public int getInternalPort() {
        return internalPort;
    }

    public void setInternalPort(int internalPort) {
        this.internalPort = internalPort;
    }

}
