package cn.nawang.ebeim.server.entity;

public class StorageCloud {

  private String id;

  private String name;

  private Long spaceCapacity;

  private Long spaceUsage;

  public void setId(String id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSpaceCapacity(Long spaceCapacity) {
    this.spaceCapacity = spaceCapacity;
  }

  public void setSpaceUsage(Long spaceUsage) {
    this.spaceUsage = spaceUsage;
  }

  public String getId() {
    return id;
  }

  @Override
  public String toString() {
    return "StorageCloud [id=" + id + ", name=" + name + ", spaceCapacity=" + spaceCapacity
            + ", spaceUsage=" + spaceUsage + "]";
  }

}
