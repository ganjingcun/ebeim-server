package cn.nawang.ebeim.server.constants;

public class Constant {

  // 临时文件前缀
  public static final String TEMP_FILE_PREFIX = "temp_file_";
  // 临时数据源文件夹名称
  public static final String TEMP_DS_FOLDER = "temp_ds_folder";
  // 文件上传路径
  public static final String FILE_UPLOAD = "/upload";
  // 文件下载路径
  public static final String FILE_DOWNLOAD = "/download";
  // zookeeper节点名称
  public static final String ZK_GROUP_ROOT="/ebeim";

}
