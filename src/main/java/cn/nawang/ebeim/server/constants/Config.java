package cn.nawang.ebeim.server.constants;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.Properties;

public class Config {

    private static final Logger LOG = LoggerFactory.getLogger(Config.class);

    public static String WORKING_DIR = null;

    public static String LOCAL_SAVE_DIR = null;

    public static String ZK_HOSTS = null;

    public static int SESSION_TIME = 0;

    public static String STORAGE_UTIL_TYPE = null;

    public static String API_URL = null;

    public static String STORAGE_CLOUD_NAME = null;

    // 上传文件端口号
    public static Integer UPLOAD_PORT = null;

    // 上传文件主机地址(外网地址)
    public static String UPLOAD_HOST = null;

    // 主机名
    public static String HOST_NAME = null;

    // 设置线程中队列的大小
    public static Integer QUEUE_SIZE = null;

    //线程池大小
    public static Integer THREAD_SIZE = null;

    // 扫描文件夹间隔
    public static Integer SCAN_INTERVAL = null;

    // swift连接信息
    public static String SWIFT_PROVIDER = null;
    public static String SWIFT_IDENTITY = null;
    public static String SWIFT_CREDENTIAL = null;
    public static String SWIFT_ENDPOINT = null;

    // 写限速
    public static Long READ_LIMIT = null;
    // 读限速
    public static Long WRITE_LIMIT = null;
    // 周期
    public static Long CHECK_INTERVAL = null;

    public static void init() throws Exception {
        try {
            String path = getConfPath() + "/log4j.properties";
            PropertyConfigurator.configure(path);

            Properties prop = new Properties();
            // 从目录“程序目录/conf”下加载配置文件
            String confPath = getConfPath() + "/config.properties";
            InputStream in = new FileInputStream(new File(confPath));
            prop.load(in);

            API_URL = prop.getProperty("api_url");
            SESSION_TIME = Integer.valueOf(prop.getProperty("session_time"));
            ZK_HOSTS = prop.getProperty("zk_hosts");

            STORAGE_UTIL_TYPE = prop.getProperty("storage_util_type");

            STORAGE_CLOUD_NAME = prop.getProperty("storage_cloud_name");
            SWIFT_PROVIDER = prop.getProperty("swift_provider");
            SWIFT_IDENTITY = prop.getProperty("swift_identity");
            SWIFT_CREDENTIAL = prop.getProperty("swift_credential");
            SWIFT_ENDPOINT = prop.getProperty("swift_endpoint");

            QUEUE_SIZE = Integer.valueOf(prop.getProperty("queue_size"));
            THREAD_SIZE = Integer.valueOf(prop.getProperty("thread_size"));
            SCAN_INTERVAL = Integer.valueOf(prop.getProperty("scan_interval"));

            WORKING_DIR = prop.getProperty("working_dir");
            LOCAL_SAVE_DIR = prop.getProperty("local_save_dir");

            READ_LIMIT = Long.valueOf(prop.getProperty("write_limit"));
            WRITE_LIMIT = Long.valueOf(prop.getProperty("read_limit"));
            CHECK_INTERVAL = Long.valueOf(prop.getProperty("check_Interval"));

            UPLOAD_PORT = Integer.valueOf(prop.getProperty("upload_port"));

            UPLOAD_HOST = prop.getProperty("upload_host");
            if (UPLOAD_HOST == null || "".equals(UPLOAD_HOST.trim())) {
                UPLOAD_HOST = InetAddress.getLocalHost().getHostAddress();
            }
            HOST_NAME = InetAddress.getLocalHost().getHostName() ;
            in.close();
        } catch (Exception e) {
            LOG.error("init config error:",e);
            throw e;
        }
    }

    private static final String CONF_PATH = "conf";

    private static String getRootPath() {
        return Class.class.getClass().getResource("/").getPath();
    }

    private static String getConfPath() {
        String rootPath = getRootPath();
        return rootPath + getChangePath(rootPath) + CONF_PATH;
    }

    private static String getChangePath(String path) {
        if (path.endsWith("classes") || path.endsWith("classes/")) {
            return "../../";
        }
        return "../";
    }

}
