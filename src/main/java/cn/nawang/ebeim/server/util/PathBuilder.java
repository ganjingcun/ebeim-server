package cn.nawang.ebeim.server.util;

import cn.nawang.ebeim.server.constants.Config;
import cn.nawang.ebeim.server.constants.Constant;

import java.io.File;

/**
 * 专门构建文件上传下载相关路径,避免程序中的大量字符串相加
 * Created by GanJc on 2015-12-30 15:40
 */
public class PathBuilder {

    public final static String FILE_DOWNLOAD_PATH =
            Config.WORKING_DIR + Constant.FILE_DOWNLOAD + File.separator + "%s" + File.separator + "%s";

    public final static String FILE_UPLOAD_PATH =
            Config.WORKING_DIR + Constant.FILE_UPLOAD + File.separator + "%s" + File.separator + "%s";

    public final static String TEMP_UPLOAD_FILE_PATH =
            Config.WORKING_DIR + Constant.FILE_UPLOAD + File.separator + "%s" + File.separator + Constant.TEMP_FILE_PREFIX + "%s";

    public static String builderFileDownloadPath(String dsId, String signature) {
        return String.format(FILE_DOWNLOAD_PATH, dsId, signature);
    }

    public static String builderFileUploadPath(String dsId, String signature) {
        return String.format(FILE_UPLOAD_PATH, dsId, signature);
    }

    public static String builderTempUploadPath(String dsId, String signature) {
        return String.format(TEMP_UPLOAD_FILE_PATH, dsId, signature);
    }

}
