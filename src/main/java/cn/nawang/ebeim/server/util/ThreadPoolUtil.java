package cn.nawang.ebeim.server.util;

import cn.nawang.ebeim.server.constants.Config;
import cn.nawang.ebeim.server.queue.Queues;
import cn.nawang.ebeim.server.thread.TaskExecutor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolUtil {

    /**
     * netty服务端线程
     */
    private static final int RECEIVE_CLIENT_CONNECT_SIZE = 1;

    /**
     * 扫描文件上传文件夹线程
     */
    private static final int SCAN_FILE_UPLOAD_FOLDER = 1;

    /**
     * 处理文件上传线程数
     */
    private static final int UPLOAD_FILE = Config.THREAD_SIZE;

    private static final int THREAD_POOL_SIZE = RECEIVE_CLIENT_CONNECT_SIZE
            + SCAN_FILE_UPLOAD_FOLDER + UPLOAD_FILE  ;

    private static ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

    public static void startUploadFileThread() {
        for (int i = 0; i < UPLOAD_FILE; i++) {
            submitTask(new TaskExecutor(Queues.uploadFileQueue));
        }
    }

    public static void submitTask(Runnable task) {
        executorService.submit(task);
    }

}
