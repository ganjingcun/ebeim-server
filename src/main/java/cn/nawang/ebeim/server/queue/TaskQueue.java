package cn.nawang.ebeim.server.queue;

import cn.nawang.ebeim.server.constants.Config;
import cn.nawang.ebeim.server.task.Task;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by GanJc on 2015-11-11 15:11
 */
public class TaskQueue<T extends Task> extends ArrayBlockingQueue<T> {

    public TaskQueue() {
        super(Config.QUEUE_SIZE);
    }

}
